import React, { FC } from 'react'
import styled, { css } from 'styled-components'

import { buttonColors } from 'styles/colors/buttonColors'
import { ReactComponent as ReloadSvg } from 'shared/img/buttons/reload.svg'
import { IStyledButtonWithIconProps, IButtonWithIconProps, scales, variants, defaultIcons, DefaultIcons } from './types'

import 'index.css'

const getSize = ({ scale = scales.MD }: IStyledButtonWithIconProps) => {
  switch (scale) {
    case scales.SM:
      return {
        width: '200px',
        height: '40px',
      }
    case scales.LG:
      return {
        width: '232px',
        height: '58px',
      }
    default:
      return {
        width: '216px',
        height: '49px',
      }
  }
}

const variantStyles = ({ variant = variants.MAIN }: IStyledButtonWithIconProps) =>
  ({
    [variants.MAIN]: css`
      color: ${buttonColors.main};
      border-color: ${buttonColors.main};
      background: transparent;
      svg {
        path {
          fill: ${buttonColors.main};
        }
      }
      :not([disabled]):hover {
        background: ${buttonColors.main};
        color: ${buttonColors.textCommon};
        svg {
          path {
            fill: ${buttonColors.textCommon};
          }
        }
      }
    `,
  }[variant])

const StyledButton = styled('button')<IStyledButtonWithIconProps>`
  height: ${props => getSize({ scale: props.scale }).height};
  // width: ${props => getSize({ scale: props.scale }).width};
  display: flex;
  align-items: center;
  padding: 14px;
  border: 2px solid transparent;
  outline: none;
  box-sizing: border-box;
  box-shadow: none;
  border-radius: 5px;

  font-family: 'Maven Pro';
  font-style: normal;
  font-weight: 600;
  font-size: 14px;

  svg {
    margin-right: 9%;
    width: 21px;
    overflow: visible;
  }

  :disabled {
    background: ${buttonColors.disabledBackground};
    border-color: ${buttonColors.disbledBorder};
    color: ${buttonColors.disabledColor};
    box-shadow: ${buttonColors.disabledBoxShadow};
    svg {
      path {
        fill: ${buttonColors.disabledColor};
      }
    }
  }

  ${variantStyles}
`

const returnDefaultIcon = (iconName: DefaultIcons) => {
  switch (iconName) {
    case defaultIcons.RELOAD:
      return <ReloadSvg />
    default:
      return <ReloadSvg />
  }
}

const ButtonWithIcon: FC<IButtonWithIconProps> = ({
  onClick,
  scale,
  variant,
  text,
  defaultIcon,
  customIcon,
  ...props
}) => {
  return (
    <StyledButton {...props} scale={scale} variant={variant} onClick={onClick}>
      {defaultIcon ? returnDefaultIcon(defaultIcon) : <></>}
      {customIcon ? customIcon : <></>}
      {text}
    </StyledButton>
  )
}

ButtonWithIcon.defaultProps = {
  scale: scales.MD,
  variant: variants.MAIN,
}

export default ButtonWithIcon
