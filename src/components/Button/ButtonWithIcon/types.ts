import React, { SVGProps } from 'react'

export enum variants {
  MAIN = 'MAIN',
}

export type Variants = keyof typeof variants

export enum scales {
  SM = 'SM',
  MD = 'MD',
  LG = 'LG',
}

export type Scales = keyof typeof scales

export interface IStyledButtonWithIconProps extends React.ComponentPropsWithoutRef<'button'> {
  scale?: Scales
  variant?: Variants
}

export enum defaultIcons {
  RELOAD = 'RELOAD',
}

export type DefaultIcons = keyof typeof defaultIcons

export interface IButtonWithIconProps extends IStyledButtonWithIconProps {
  onClick: () => void
  text: string
  defaultIcon?: DefaultIcons
  customIcon?: SVGProps<SVGSVGElement>
}
