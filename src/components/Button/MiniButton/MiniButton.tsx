import React, { FC } from 'react'
import styled, { css } from 'styled-components'

import { buttonColors } from 'styles/colors/buttonColors'
import { ReactComponent as DeleteSvg } from 'shared/img/buttons/delete.svg'
import { ReactComponent as EditSvg } from 'shared/img/buttons/edit.svg'
import { ReactComponent as HistorySvg } from 'shared/img/buttons/history.svg'
import { IStyledMiniButtonProps, IMiniButtonProps, scales, variants, defaultIcons, DefaultIcons } from './types'

import 'index.css'

const getSize = ({ scale = scales.MD }: IStyledMiniButtonProps) => {
  switch (scale) {
    case scales.SM:
      return {
        width: '75px',
        height: '33px',
      }
    case scales.LG:
      return {
        width: '75px',
        height: '33px',
      }
    default:
      return {
        width: '75px',
        height: '33px',
      }
  }
}

const variantStyles = ({ variant = variants.MAIN }: IStyledMiniButtonProps) =>
  ({
    [variants.MAIN]: css`
      color: ${buttonColors.main};
      background: ${buttonColors.miniButtonMainOpacity};
      svg {
        path {
          fill: ${buttonColors.main};
        }
      }
      :not([disabled]):hover {
        background: ${buttonColors.main};
        color: ${buttonColors.textCommon};
        svg {
          path {
            fill: ${buttonColors.textCommon};
          }
        }
      }
    `,
    [variants.DANGER]: css`
      color: ${buttonColors.danger};
      background: ${buttonColors.miniButtonDangerOpacity};
      svg {
        path {
          fill: ${buttonColors.danger};
        }
      }
      :not([disabled]):hover {
        background: ${buttonColors.danger};
        color: ${buttonColors.textCommon};
        svg {
          path {
            fill: ${buttonColors.danger};
          }
        }
      }
    `,
  }[variant])

const StyledButton = styled('button')<IStyledMiniButtonProps>`
  height: ${props => getSize({ scale: props.scale }).height};
  // width: ${props => getSize({ scale: props.scale }).width};
  display: flex;
  align-items: center;
  padding: 9px 4px 9px 4px;
  box-shadow: none;
  border-radius: 5px;
  outline: none;
  font-family: 'Maven Pro';
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  border: none;

  svg {
    margin-right: 9px;
    width: 24px;
    overflow: visible;
  }

  :disabled {
    background: ${buttonColors.disabledBackground};
    color: ${buttonColors.disabledColor};
    svg {
      path {
        fill: ${buttonColors.disabledColor};
      }
    }
  }

  ${variantStyles}
`

const returnDefaultIcon = (iconName: DefaultIcons) => {
  switch (iconName) {
    case defaultIcons.DELETE:
      return <DeleteSvg />
    case defaultIcons.EDIT:
      return <EditSvg />
    case defaultIcons.HISTORY:
      return <HistorySvg />
    default:
      return <EditSvg />
  }
}

const MiniButton: FC<IMiniButtonProps> = ({ onClick, scale, variant, text, defaultIcon, customIcon, ...props }) => {
  return (
    <StyledButton {...props} scale={scale} variant={variant} onClick={onClick}>
      {defaultIcon ? returnDefaultIcon(defaultIcon) : <></>}
      {customIcon ? customIcon : <></>}
      {text}
    </StyledButton>
  )
}

MiniButton.defaultProps = {
  scale: scales.MD,
  variant: variants.MAIN,
}

export default MiniButton
