import React, { SVGProps } from 'react'

export enum variants {
  MAIN = 'MAIN',
  DANGER = 'DANGER',
}

export type Variants = keyof typeof variants

export enum scales {
  SM = 'SM',
  MD = 'MD',
  LG = 'LG',
}

export type Scales = keyof typeof scales

export interface IStyledMiniButtonProps extends React.ComponentPropsWithoutRef<'button'> {
  scale?: Scales
  variant?: Variants
}

export enum defaultIcons {
  EDIT = 'EDIT',
  DELETE = 'DELETE',
  HISTORY = 'HISTORY',
}

export type DefaultIcons = keyof typeof defaultIcons

export interface IMiniButtonProps extends IStyledMiniButtonProps {
  onClick: () => void
  text: string
  defaultIcon?: DefaultIcons
  customIcon?: SVGProps<SVGSVGElement>
}
