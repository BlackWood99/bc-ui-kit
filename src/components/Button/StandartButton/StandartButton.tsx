import React, { FC } from 'react'
import styled, { css } from 'styled-components'

import { buttonColors } from 'styles/colors/buttonColors'
import { IStyledStandartButtonProps, IStandartButtonProps, scales, variants } from './types'
import 'index.css'

const getSize = ({ scale = scales.MD }: IStyledStandartButtonProps) => {
  switch (scale) {
    case scales.SM:
      return {
        width: '80px',
        height: '33px',
      }
    case scales.LG:
      return {
        width: '120px',
        height: '52px',
      }
    default:
      return {
        width: '109px',
        height: '46px',
      }
  }
}

const variantStyles = ({ variant = variants.MAIN }: IStyledStandartButtonProps) =>
  ({
    [variants.MAIN]: css`
      color: ${buttonColors.main};
      border-color: ${buttonColors.main};
      background: transparent;
      :not([disabled]):hover {
        background: ${buttonColors.main};
        color: ${buttonColors.textCommon};
      }
    `,
    [variants.SUCCESS]: css`
      color: ${buttonColors.success};
      border-color: ${buttonColors.success};
      background: transparent;
      :not([disabled]):hover {
        background: ${buttonColors.success};
        color: ${buttonColors.textCommon};
      }
    `,
    [variants.DANGER]: css`
      color: ${buttonColors.danger};
      border-color: ${buttonColors.danger};
      background: transparent;
      :not([disabled]):hover {
        background: ${buttonColors.danger};
        color: ${buttonColors.textCommon};
      }
    `,
  }[variant])

const StyledButton = styled('button')<IStyledStandartButtonProps>`
  height: ${props => getSize({ scale: props.scale }).height};
  // width: ${props => getSize({ scale: props.scale }).width};
  display: flex;
  align-items: center;
  justify-content: center;
  border: 2px solid transparent;
  outline: none;
  box-sizing: border-box;
  box-shadow: none;
  border-radius: 5px;

  font-family: 'Maven Pro';
  font-style: normal;
  font-weight: 600;
  font-size: 14px;

  :disabled {
    background: ${buttonColors.disabledBackground};
    border-color: ${buttonColors.disbledBorder};
    color: ${buttonColors.disabledColor};
    box-shadow: ${buttonColors.disabledBoxShadow};
  }

  ${variantStyles};
`

const StandartButton: FC<IStandartButtonProps> = ({ onClick, scale, variant, text, ...props }) => {
  return (
    <StyledButton {...props} scale={scale} variant={variant} onClick={onClick}>
      {text}
    </StyledButton>
  )
}

StandartButton.defaultProps = {
  scale: scales.MD,
  variant: variants.MAIN,
}

export default StandartButton
