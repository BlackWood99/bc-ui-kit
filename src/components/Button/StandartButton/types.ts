import React from 'react'

export enum variants {
  MAIN = 'MAIN',
  SUCCESS = 'SUCCESS',
  DANGER = 'DANGER',
}

export type Variants = keyof typeof variants

export enum scales {
  SM = 'SM',
  MD = 'MD',
  LG = 'LG',
}

export type Scales = keyof typeof scales

export interface IStyledStandartButtonProps extends React.ComponentPropsWithoutRef<'button'> {
  scale?: Scales
  variant?: Variants
}

export interface IStandartButtonProps extends IStyledStandartButtonProps {
  onClick: () => void
  text: string
}
