import React, { FC } from 'react'
import styled, { css } from 'styled-components'

import { chartHoverColors } from 'styles/colors/chartHoverColors'
import {ReactComponent as TriangleSvg} from 'shared/img/chartHover/triangle.svg'
import {ReactComponent as TriangleWithBorderSvg} from 'shared/img/chartHover/triangleWithBorder.svg'
import { IStyledChartHoverProps, IChartHoverProps, scales, variants } from './types'
import '../../index.css'

const getSize = ({ scale = scales.MD }: IStyledChartHoverProps) => {
  switch (scale) {
    case scales.SM:
      return {
        width: '200px',
        height: '100px',
      }
    case scales.LG:
      return {
        width: '200px',
        height: '100px',
      }
    default:
      return {
        width: '200px',
        height: '100px',
      }
  }
}

const variantStyles = ({ variant = variants.MAIN }: IStyledChartHoverProps) =>
  ({
    [variants.MAIN]: css`
      color: ${chartHoverColors.mainText};
      border-color: ${chartHoverColors.mainBorder};
      background: ${chartHoverColors.mainBackground};
      .title-text-container {
        .additional-text {
          color: ${chartHoverColors.mainAdditionalText};
        }
      }

      .hide-triangle {
        background: ${chartHoverColors.mainBackground};
      }
    `,
  }[variant])

const ChartHoverContainer = styled('div')<IStyledChartHoverProps>`
  height: ${props => getSize({ scale: props.scale }).height};
  width: ${props => getSize({ scale: props.scale }).width};
  display: ${props => (props.isVisible ? 'flex' : 'none')};
  flex-direction: column;
  align-items: center;
  padding: 5px 0;
  border: ${props => props.bordered ? '1px solid transparent' : ''};
  outline: none;
  box-sizing: border-box;
  box-shadow: none;
  border-radius: 10px;
  position: relative;
  z-index: 2;
  transition: 0.3s ease-out;

  font-family: 'Maven Pro';
  font-style: normal;
  font-weight: 600;
  font-size: 24px;

  .title-text-container {
    font-size: 12px;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 5px;
    margin-bottom: 11px;
  }

  .circle {
    height: 17px;
    width: 17px;
    border-radius: 50%;
    background: ${props => props.circleColor ? props.circleColor : chartHoverColors.mainCircle};
  }

  svg {
    position: absolute;
    z-index: 1;
    top: -17px;
    height: 27px;
    right: 14px;
  }

  .hide-triangle {
    position: absolute;
    z-index: 2;
    top: -0.02px; //так и должно быть, иначе видно часть треугольника
    width: 27px;
    height: 10px;
    right: 14px;
  }

  ${variantStyles};
`

const ChartHover: FC<IChartHoverProps> = ({ title, value, additionalText, isVisible, variant, scale, bordered, circleColor }) => {
  return (
    <ChartHoverContainer isVisible={isVisible} scale={scale} variant={variant} bordered={bordered} circleColor={circleColor}>
        {bordered ? <TriangleWithBorderSvg/> : <TriangleSvg/>}
        <div className='hide-triangle'/>
      <div className="circle" />
      <div className="title-text-container">
        <span>{title}</span>
        <span className="additional-text">{additionalText ? additionalText : ''}</span>
      </div>
      <span>{value}</span>
    </ChartHoverContainer>
  )
}

ChartHover.defaultProps = {
  scale: scales.MD,
  variant: variants.MAIN,
}

export default ChartHover
