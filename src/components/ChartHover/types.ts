export enum variants {
  MAIN = 'MAIN',
}

export type Variants = keyof typeof variants

export enum scales {
  SM = 'SM',
  MD = 'MD',
  LG = 'LG',
}

export type Scales = keyof typeof scales

export interface IStyledChartHoverProps {
  scale?: Scales
  variant?: Variants
  isVisible?: boolean
  bordered?: boolean
  circleColor?: string
}

export interface IChartHoverProps extends IStyledChartHoverProps {
  title: string
  value: number
  additionalText?: string
  isVisible: boolean //костыль
}
