import styled from 'styled-components'
import { ICheckboxProps, scales } from './types'

const getScale = ({ scale }: ICheckboxProps) => {
  switch (scale) {
    case scales.SM:
      return '24px'
    case scales.MD:
    default:
      return '20px'
  }
}

const Checkbox = styled('input').attrs({ type: 'checkbox' })<ICheckboxProps>`
  appearance: none;
  overflow: hidden;
  cursor: pointer;
  position: relative;
  display: inline-block;
  height: ${getScale};
  width: ${getScale};
  vertical-align: middle;
  transition: background-color 0.2s ease-in-out;
  border-radius: 5px;
  background-color: transparent;
  border: 1px solid rgba(197, 198, 200, 1);

  &:after {
    content: '';
    position: absolute;
    border-bottom: 3px solid;
    border-left: 3px solid;
    border-radius: 3px;
    border-color: transparent;
    top: 25%;
    left: 0;
    right: 0;
    width: 40%;
    height: 20%;
    margin: auto;
    transform: rotate(-50deg);
    transition: border-color 0.2s ease-in-out;
  }

  &:hover:not(:disabled):not(:checked) {
  }

  &:focus {
    outline: none;
  }

  &:checked {
    background-color: rgba(70, 162, 159, 1);
    &:after {
      border-color: white;
    }
  }

  &:disabled {
    cursor: default;
    opacity: 0.6;
  }
`

Checkbox.defaultProps = {
  scale: scales.MD,
}

export default Checkbox
