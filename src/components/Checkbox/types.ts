export enum scales {
  SM = 'SM',
  MD = 'MD',
}

export type Scales = keyof typeof scales

export interface ICheckboxProps {
  scale?: Scales
}
