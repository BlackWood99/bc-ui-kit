import React, { useMemo } from 'react'
import styled, { css, createGlobalStyle } from 'styled-components'
import RDatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

import { IDatePickerProps } from './types'
import InputGroup from 'components/Input/InputGroup'
import { getInputHeight } from 'components/Input/Input'
import { Scales } from 'components/Input/types'

import { ReactComponent as ArrowDownIcon } from 'shared/img/arrows/down.svg'
import { ReactComponent as CalendarIcon } from 'shared/img/calendar.svg'

interface IDatePickerWrapperProps {
  scale?: Scales
}

const DatePickerWrapperStyles = createGlobalStyle<IDatePickerWrapperProps>`
    .date_picker {
      position: relative;
      z-index: 2;
      box-sizing: border-box;
      width: 100%;
      height: ${({ scale }) => getInputHeight(scale)};
    }

    .react-datepicker {
      background: rgba(32, 40, 51, 1);

      &__triangle {
        left: 20px !important;
        transform: translate(0px, 0px) !important;
      }

      &__input-container, &__input-container > input {
        box-sizing: border-box;
        height: 100%;
        width: 100%;
      }
      &__input-container > input {
        outline: none;
        border: 1px solid #c5c6c8;
        width: 100%;
        height: 100%;
        background-color: transparent;
        
        font-family: 'Maven Pro';
        font-size: 14px;
        font-weight: 600;
        line-height: 16px;
        letter-spacing: 0em;
        text-align: center;
        color: #fff;
      }
      
      &__header {
        background: transparent;
        * {
          color: #fff;
        }
      }

      &__day {
        color: #fff;

        &:hover {
          color: #000;
        }
      }
    }
`

const DatePicker: React.FC<IDatePickerProps> = props => {
  const { formatView } = props

  const dateFormat = useMemo(() => (formatView ? formatView : 'dd.MM.yyyy HH:mm:ss'), [formatView])

  return (
    <>
      <InputGroup startIcon={<CalendarIcon />} endIcon={<ArrowDownIcon />} {...props.wrapperProps}>
        <RDatePicker wrapperClassName="date_picker" dateFormat={dateFormat} {...props} />
      </InputGroup>
      <DatePickerWrapperStyles scale={props.scale} />
    </>
  )
}

export default DatePicker
