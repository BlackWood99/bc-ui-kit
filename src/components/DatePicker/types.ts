import { ReactDatePickerProps } from 'react-datepicker'

import { IInputGroupWithoutChildProps, Scales } from 'components/Input/types'

export interface IDatePickerProps extends ReactDatePickerProps {
  scale?: Scales
  formatView?: string
  wrapperProps?: IInputGroupWithoutChildProps
}
