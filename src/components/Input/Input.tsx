import React from 'react'
import styled, { css } from 'styled-components'

import { IInputProps, Scales, scales, variants } from './types'
import 'index.css'

export const getInputHeight = (scale?: Scales) => {
  switch (scale) {
    case scales.SM:
      return '24px'
    case scales.MD:
      return '44px'
    case scales.LG:
    default:
      return '32px'
  }
}

const variantStyles = ({ variant = variants.MAIN }: IInputProps) =>
  ({
    [variants.MAIN]: css`
      color: #c5c6c8;
      border-color: #c5c6c8;
    `,
    [variants.SUCCESS]: css`
      color: #fff;
      border-color: #11c023;
      border-width: 2px;
    `,
    [variants.DANGER]: css`
      color: #fff;
      border-color: #c01e1e;
      border-width: 2px;
    `,
    [variants.SECONDARY]: css`
      color: #c5c6c8;
      border-color: #66fcf1;
      border-width: 2px;
    `,
    [variants.FILLED]: css`
      color: #c5c6c8;
      border-color: #c5c6c8;
      background: rgba(197, 198, 200, 0.4);

      &::placeholder {
        color: #fff;
        opacity: 0.2;
      }
    `,
  }[variant])

export const StyledInput = styled('input')<IInputProps>`
  height: ${({ scale }) => getInputHeight(scale)};
  background: transparent;
  padding: 10px 16px;
  border: 1px solid transparent;
  border-radius: 5px;
  box-shadow: none;
  outline: none;
  box-sizing: border-box;
  width: 100%;

  font-family: 'Maven Pro';
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: ${({ scale }) => getInputHeight(scale)};

  &::placeholder {
    opacity: 0.7;
  }

  &:disabled {
    opacity: 0.7;
    cursor: not-allowed;
  }

  &:focus:not(:disabled) {
  }

  ${variantStyles};

  color: ${({ color }) => color || '#c5c6c8'};
`

const Input: React.FC<IInputProps> = props => {
  return <StyledInput {...props} />
}

Input.defaultProps = {
  scale: scales.MD,
  variant: variants.MAIN,
  color: undefined,
}

export default Input
