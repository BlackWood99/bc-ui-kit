import React, { cloneElement } from 'react'
import styled from 'styled-components'

import { StyledInput } from './Input'
import { IInputGroupProps, scales, Scales, variants } from './types'

export const getInputPadding = (scale: Scales, hasIcon: boolean) => {
  if (!hasIcon) {
    return '16px'
  }

  switch (scale) {
    case scales.SM:
      return '24px'
    case scales.MD:
      return '44px'
    case scales.LG:
    default:
      return '32px'
  }
}

interface IStyledInputGroupProps extends React.CSSProperties {
  scale: Scales
  hasStartIcon: boolean
  hasEndIcon: boolean
}

const StyledInputGroup = styled('div')<IStyledInputGroupProps>`
  box-sizing: border-box;
  position: relative;

  ${StyledInput}, .react-datepicker__input-container > input {
    padding-left: ${({ hasStartIcon, scale }) => getInputPadding(scale, hasStartIcon)};
    padding-right: ${({ hasEndIcon, scale }) => getInputPadding(scale, hasEndIcon)};
  }
`

const InputIcon = styled.div<{ scale: Scales; iconColor?: string; isEndIcon?: boolean }>`
  align-items: center;
  display: flex;
  height: 100%;
  position: absolute;
  top: 0;
  box-sizing: border-box;

  ${({ isEndIcon, scale }) =>
    isEndIcon
      ? `
    right: ${scale === scales.SM ? '8px' : '16px'};
  `
      : `
    left: ${scale === scales.SM ? '8px' : '16px'};
  `}
  ${({ iconColor }) =>
    iconColor
      ? `
    & > svg > * { fill: ${iconColor}; } 
    color: ${iconColor};
  `
      : null};
`

const InputGroup: React.FC<IInputGroupProps> = ({
  scale = scales.MD,
  variant = variants.MAIN,
  startIcon,
  endIcon,
  iconColor,
  textColor,
  children,
  ...props
}): JSX.Element => (
  <StyledInputGroup scale={scale} hasStartIcon={!!startIcon} hasEndIcon={!!endIcon} {...props}>
    {startIcon && (
      <InputIcon scale={scale} iconColor={iconColor}>
        {startIcon}
      </InputIcon>
    )}
    {cloneElement(children, { scale, variant, color: textColor })}
    {endIcon && (
      <InputIcon scale={scale} iconColor={iconColor} isEndIcon>
        {endIcon}
      </InputIcon>
    )}
  </StyledInputGroup>
)

export default InputGroup
