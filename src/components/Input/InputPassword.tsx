import React, { useState } from 'react'
import styled from 'styled-components'

import Input, { StyledInput } from './Input'
import { InputPasswordProps, scales, Scales } from './types'

import { ReactComponent as EyeIcon } from 'shared/img/eye.svg'
import { ReactComponent as EyeClosedIcon } from 'shared/img/eyeClosed.svg'

const getPadding = (scale: Scales) => {
  switch (scale) {
    case scales.SM:
      return '24px'
    case scales.MD:
      return '44px'
    case scales.LG:
    default:
      return '32px'
  }
}

interface IStyledInputPasswordProps extends React.CSSProperties {
  scale: Scales
}

const StyledInputPassword = styled('div')<IStyledInputPasswordProps>`
  box-sizing: border-box;
  position: relative;

  ${StyledInput} {
    padding-right: ${({ scale }) => getPadding(scale)};
  }
`

const InputIcon = styled.div<{ scale: Scales }>`
  display: flex;
  align-items: center;
  height: 100%;
  box-sizing: border-box;
  position: absolute;
  top: 0;
  right: ${({ scale }) => (scale === scales.SM ? '8px' : '16px')};
`

const InputPassword: React.FC<InputPasswordProps> = ({ scale = scales.MD, ...props }): JSX.Element => {
  const [isHidden, setIsHidden] = useState(true)

  const onToggleIcon = () => setIsHidden(prev => !prev)

  return (
    <StyledInputPassword scale={scale}>
      <Input scale={scale} type={isHidden ? 'password' : 'text'} {...props} />
      <InputIcon onClick={onToggleIcon} scale={scale}>
        {isHidden ? <EyeIcon /> : <EyeClosedIcon />}
      </InputIcon>
    </StyledInputPassword>
  )
}

export default InputPassword
