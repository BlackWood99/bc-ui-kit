export enum variants {
  MAIN = 'MAIN',
  SUCCESS = 'SUCCESS',
  DANGER = 'DANGER',
  SECONDARY = 'SECONDARY',
  FILLED = 'FILLED',
}

export type Variants = keyof typeof variants

export enum scales {
  SM = 'SM',
  MD = 'MD',
  LG = 'LG',
}

export type Scales = keyof typeof scales

export interface IInputProps extends React.ComponentPropsWithoutRef<'input'> {
  scale?: Scales
  variant?: Variants
  color?: string
}

export interface IInputGroupProps extends React.HTMLAttributes<HTMLElement> {
  scale?: Scales
  variant?: Variants
  startIcon?: React.ReactElement
  endIcon?: React.ReactElement
  iconColor?: string
  textColor?: string
  children: JSX.Element
}

export interface IInputGroupWithoutChildProps extends Omit<IInputGroupProps, 'children'> {
  children?: null
}

export interface InputPasswordProps extends React.ComponentPropsWithoutRef<'input'> {
  scale?: Scales
}
