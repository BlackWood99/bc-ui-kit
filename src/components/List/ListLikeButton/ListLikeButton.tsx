import React, { FC, useState } from 'react'
import styled, { css } from 'styled-components'
import { AnimatePresence, motion } from 'framer-motion'

import { listColors } from 'styles/colors/listColors'
import { ReactComponent as LanguageSvg } from 'shared/img/lists/language.svg'
import { ReactComponent as ArrowSvg } from 'shared/img/arrows/down.svg'
import { IStyledListLikeButtonProps, IListLikeButtonProps, scales, variants, defaultIcon, DefaultIcon } from './types'

import '../../../index.css'

const getSize = ({ scale = scales.MD }: IStyledListLikeButtonProps) => {
  switch (scale) {
    case scales.SM:
      return {
        height: '25px',
      }
    case scales.LG:
      return {
        height: '45px',
      }
    default:
      return {
        height: '33px',
      }
  }
}

const listItemVariantsStyles = ({ variant = variants.MAIN, selected }: IStyledListLikeButtonProps) =>
  ({
    [variants.MAIN]: css`
      color: ${selected ? listColors.mainText : listColors.itemText};
      background: ${selected ? listColors.mainItemSelectedBackground : listColors.mainItemBackground};
      :last-child {
        border-radius: 0 0 5px 5px;
      }
      :hover {
        background: ${selected ? listColors.mainItemSelectedBackground : listColors.mainItemHoverBackground};
      }
    `,
  }[variant])

const listHeaderVariantStyles = ({ variant = variants.MAIN, isOpen }: IStyledListLikeButtonProps) =>
  ({
    [variants.MAIN]: css`
      color: ${listColors.mainText};
      background: transparent;
      border-radius: ${isOpen ? '5px 5px 0 0' : '5px'};
      :hover {
        background: ${listColors.mainListHeaderBackground};
      }
      svg {
        fill: ${listColors.mainText};
        path {
          fill: ${listColors.mainText};
        }
      }
      .arrow-icon {
        transform: ${isOpen ? 'rotate(180deg)' : ''};
        width: 16px;
      }
    `,
  }[variant])

const ListContainer = styled('div')<IStyledListLikeButtonProps>`
  width: 100%;
  position: relative;

  font-family: 'Maven Pro';
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
`

const ListHeader = styled('div')<IStyledListLikeButtonProps>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  width: 100%;
  height: ${props => getSize({ scale: props.scale }).height};
  padding: 4px;
  text-transform: capitalize;

  .text-with-icon {
    display: flex;
    align-items: center;
    max-width: 90%;

    span {
      white-space: nowrap;
      text-overflow: ellipsis;
      overflow: hidden;
      display: inline;
    }
    svg {
      margin-right: 9px;
      width: 25px;
      overflow: visible;
    }
  }

  ${listHeaderVariantStyles}
`

const ListItemsContainer = styled(motion.ul)<IStyledListLikeButtonProps>`
  width: 100%;
  position: absolute;
  background: transparent;
  padding: 0;
  margin: 0;
  max-height: 200px;
  overflow-y: auto;
`

const ListItem = styled('li')<IStyledListLikeButtonProps>`
  width: 100%;
  list-style: none;
  display: flex;
  align-items: center;
  box-sizing: border-box;
  height: ${props => getSize({ scale: props.scale }).height};
  padding: 4px;
  text-transform: capitalize;

  ${listItemVariantsStyles}
`

const returnDefaultIcon = (iconName: DefaultIcon) => {
  switch (iconName) {
    case defaultIcon.LANGUAGE:
      return <LanguageSvg />
    default:
      return <LanguageSvg />
  }
}

const ListLikeButton: FC<IListLikeButtonProps> = ({
  scale,
  variant,
  placeHolder,
  elements,
  defaultIcon,
  customIcon,
  value,
  onChange,
  displayPlaceHolder = false,
}) => {
  const [isOpen, setIsOpen] = useState<boolean>(false)

  const changeListState = () => {
    setIsOpen(!isOpen)
  }

  const onListItemClick = (item: string) => {
    onChange(item)
    setIsOpen(false)
  }

  const returnSelected = (item: string) => {
    if (item === value) {
      return true
    }
    return false
  }

  return (
    <ListContainer scale={scale}>
      <ListHeader isOpen={isOpen} onClick={changeListState} scale={scale} variant={variant}>
        <div className="text-with-icon">
          {defaultIcon ? returnDefaultIcon(defaultIcon) : <></>}
          {customIcon ? customIcon : <></>}
          {displayPlaceHolder && value ? `${placeHolder}: ` : ''}
          {/* если выбрано отображение плейсхолдера и есть значение - отображаем плейсхолдер и двоеточие перед значением */}
          {value ? <span>{value}</span> : placeHolder}
        </div>
        <ArrowSvg className="arrow-icon" />
      </ListHeader>
      <AnimatePresence initial={false}>
        {isOpen && (
          <ListItemsContainer
            scale={scale}
            key="content"
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: { opacity: 1, height: 'auto', transition: { duration: 0.35, ease: [0.04, 0.62, 0.83, 0.98] } },
              collapsed: { opacity: 0, height: 0, transition: { duration: 0.2, ease: 'easeOut' } },
            }}>
            {elements.map((element, index) => (
              <ListItem
                key={index}
                onClick={() => onListItemClick(element)}
                selected={returnSelected(element)}
                scale={scale}
                variant={variant}>
                {element}
              </ListItem>
            ))}
          </ListItemsContainer>
        )}
      </AnimatePresence>
    </ListContainer>
  )
}

ListLikeButton.defaultProps = {
  scale: scales.MD,
  variant: variants.MAIN,
}

export default ListLikeButton
