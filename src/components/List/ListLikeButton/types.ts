import React, { SVGProps } from 'react'

export enum variants {
  MAIN = 'MAIN',
}

export type Variants = keyof typeof variants

export enum scales {
  SM = 'SM',
  MD = 'MD',
  LG = 'LG',
}

export type Scales = keyof typeof scales

export enum defaultIcon {
  LANGUAGE = 'LANGUAGE',
}

export type DefaultIcon = keyof typeof defaultIcon

export interface IStyledListLikeButtonProps {
  scale?: Scales
  variant?: Variants
  isOpen?: boolean
  selected?: boolean
}

export interface IListLikeButtonProps extends IStyledListLikeButtonProps {
  placeHolder: string
  customIcon?: SVGProps<SVGSVGElement>
  defaultIcon?: DefaultIcon
  elements: string[]
  value: string
  onChange: (item: string) => void
  displayPlaceHolder?: boolean
}
