import React, { FC, useState } from 'react'
import styled, { css } from 'styled-components'
import { AnimatePresence, motion } from 'framer-motion'

import { listColors } from 'styles/colors/listColors'
import { ReactComponent as PersonSvg } from 'shared/img/lists/person.svg'
import { ReactComponent as ArrowSvg } from 'shared/img/arrows/down.svg'
import { IStyledListLikeFieldProps, IListLikeFieldProps, scales, variants, defaultIcon, DefaultIcon } from './types'

import '../../../index.css'

const getSize = ({ scale = scales.MD }: IStyledListLikeFieldProps) => {
  switch (scale) {
    case scales.SM:
      return {
        width: '300px',
        height: '30px',
      }
    case scales.LG:
      return {
        width: '380px',
        height: '55px',
      }
    default:
      return {
        width: '354px',
        height: '45px',
      }
  }
}

const listItemVariantsStyles = ({ variant = variants.MAIN, selected }: IStyledListLikeFieldProps) =>
  ({
    [variants.MAIN]: css`
      color: ${selected ? listColors.mainText : listColors.itemText};
      background: ${selected ? listColors.mainItemSelectedBackground : listColors.mainItemBackground};
      border-color: ${listColors.mainBorder};
      :last-child {
        border-radius: 0 0 5px 5px;
      }
      :hover {
        background: ${selected ? listColors.mainItemSelectedBackground : listColors.mainItemHoverBackground};
      }
    `,
  }[variant])

const listHeaderVariantStyles = ({ variant = variants.MAIN, isOpen }: IStyledListLikeFieldProps) =>
  ({
    [variants.MAIN]: css`
      color: ${listColors.mainText};
      background: transparent;
      border-color: ${listColors.mainBorder};
      border-radius: ${isOpen ? '5px 5px 0 0' : '5px'};
      svg {
        fill: ${listColors.mainText};
        path {
            fill: ${listColors.mainText};
        }
      }
      .arrow-icon {
        transform: ${isOpen ? 'rotate(180deg)' : ''};
        width: 16px;
      }
    `,
  }[variant])

const ListContainer = styled('div')<IStyledListLikeFieldProps>`
  width: ${props => getSize({ scale: props.scale }).width};
  position: relative;

  font-family: 'Maven Pro';
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
`

const ListHeader = styled('div')<IStyledListLikeFieldProps>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  width: 100%;
  height: ${props => getSize({ scale: props.scale }).height};
  padding: 10.5px 17px;
  border: 1px solid transparent;
  text-transform: capitalize;

  .text-with-icon {
    display: flex;
    align-items: center;
    svg {
      margin-right: 9px;
      width: 16px;
      overflow: visible;
    }
  }

  ${listHeaderVariantStyles}
`

const ListItemsContainer = styled(motion.ul)<IStyledListLikeFieldProps>`
  width: ${props => getSize({ scale: props.scale }).width};
  position: absolute;
  background: transparent;
  padding: 0;
  margin: 0;
  max-height: 250px;
  overflow-y: auto;
`

const ListItem = styled('li')<IStyledListLikeFieldProps>`
  width: 100%;
  list-style: none;
  display: flex;
  align-items: center;
  box-sizing: border-box;
  height: ${props => getSize({ scale: props.scale }).height};
  padding: 10px 17px;
  border: 1px solid transparent;
  text-transform: capitalize;
  :not(:last-child) {
    border-bottom: none;
  }
  :first-child {
    border-top: none;
  }
  ${listItemVariantsStyles}
`

const returnDefaultIcon = (iconName: DefaultIcon) => {
  switch (iconName) {
    case defaultIcon.PERSON:
      return <PersonSvg />
    default:
      return <PersonSvg />
  }
}

const ListLikeField: FC<IListLikeFieldProps> = ({ scale, variant, placeHolder, elements, defaultIcon, customIcon, value, onChange }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false)

  const changeListState = () => {
    setIsOpen(!isOpen)
  }

  const onListItemClick = (item: string) => {
    onChange(item)
    setIsOpen(false)
  }

  const returnIsSelected = (item: string) => {
    if (item === value) {
      return true
    }
    return false
  }

  return (
    <ListContainer scale={scale}>
      <ListHeader isOpen={isOpen} onClick={changeListState} scale={scale} variant={variant}>
        <div className="text-with-icon">
          {defaultIcon ? returnDefaultIcon(defaultIcon) : <></>}
          {customIcon ? customIcon : <></>}
          {value ? value : placeHolder}
        </div>
        <ArrowSvg className="arrow-icon" />
      </ListHeader>
      <AnimatePresence initial={false}>
        {isOpen && (
          <ListItemsContainer
            scale={scale}
            key="content"
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: { opacity: 1, height: 'auto', transition: { duration: 0.35, ease: [0.04, 0.62, 0.83, 0.98] } },
              collapsed: { opacity: 0, height: 0, transition: { duration: 0.2, ease: 'easeOut' } },
            }}>
            {elements.map((element, index) => (
              <ListItem
                key={index}
                onClick={() => onListItemClick(element)}
                selected={returnIsSelected(element)}
                scale={scale}
                variant={variant}>
                {element}
              </ListItem>
            ))}
          </ListItemsContainer>
        )}
      </AnimatePresence>
    </ListContainer>
  )
}

ListLikeField.defaultProps = {
  scale: scales.MD,
  variant: variants.MAIN,
}

export default ListLikeField
