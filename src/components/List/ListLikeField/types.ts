import React, { SVGProps } from 'react'

export enum variants {
  MAIN = 'MAIN',
}

export type Variants = keyof typeof variants

export enum scales {
  SM = 'SM',
  MD = 'MD',
  LG = 'LG',
}

export type Scales = keyof typeof scales

export enum defaultIcon {
  PERSON = 'PERSON',
}

export type DefaultIcon = keyof typeof defaultIcon

export interface IStyledListLikeFieldProps {
  scale?: Scales
  variant?: Variants
  isOpen?: boolean
  selected?: boolean
}

export interface IListLikeFieldProps extends IStyledListLikeFieldProps {
  placeHolder: string
  customIcon?: SVGProps<SVGSVGElement>
  defaultIcon?: DefaultIcon
  elements: string[]
  value: string
  onChange: (item: string) => void
}
