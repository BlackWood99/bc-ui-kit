import React, { FC, useCallback, useEffect, useState } from 'react'
import styled, { css } from 'styled-components'
import ReactPaginate from 'react-paginate'

import { paginatorColors } from 'styles/colors/paginatorColors'
import { ReactComponent as LeftArrowSvg } from 'shared/img/arrows/left.svg'
import { ReactComponent as RightArrowSvg } from 'shared/img/arrows/right.svg'
import { IStyledPaginatorProps, IPaginatorProps, scales, variants } from './types'
import '../../index.css'

const getSize = ({ scale = scales.MD }: IStyledPaginatorProps) => {
  switch (scale) {
    case scales.SM:
      return {
        width: '1572px',
        height: '116px',
      }
    case scales.LG:
      return {
        width: '1572px',
        height: '116px',
      }
    default:
      return {
        width: '1572px',
        height: '116px',
      }
  }
}

const paginatorVariantStyles = ({ variant = variants.MAIN }: IStyledPaginatorProps) =>
  ({
    [variants.MAIN]: css`
      color: ${paginatorColors.mainText};
      background: ${paginatorColors.mainBackground};

      .total-container {
        .number-container {
          border-color: ${paginatorColors.mainText};
        }
      }

      svg {
        path {
          fill: ${paginatorColors.mainText};
        }
      }
    `,
  }[variant])

const selectedItemVariantStyles = ({ variant = variants.MAIN }: IStyledPaginatorProps) =>
  ({
    [variants.MAIN]: css`
      a {
        color: ${paginatorColors.mainSelectedItemColor};
        background: ${paginatorColors.mainSelectedItemBackground};
      }
    `,
  }[variant])

const PaginatorContainer = styled('div')<IStyledPaginatorProps>`
  height: ${props => getSize({ scale: props.scale }).height};
  width: ${props => getSize({ scale: props.scale }).width};
  display: flex;
  align-items: center;
  padding: 29px 46px;
  outline: none;
  box-sizing: border-box;
  box-shadow: none;
  border-radius: 20px;

  font-family: 'Maven Pro';
  font-style: normal;
  font-weight: 600;
  font-size: ${props => (props.pageSelected! >= 98 ? (props.pageSelected! >= 996 ? '20px' : '24px') : '32px')};

  .total-container {
    display: flex;
    align-items: center;
    margin-left: 60px;
    margin-left: 60px;
    .number-container {
      display: flex;
      align-items: center;
      justify-content: center;
      border: 1px solid transparent;
      border-radius: 10px;
      padding: 10px;
      margin-left: 10px;
    }
  }

  ul {
    display: flex;
    align-items: center;
    margin: 0;
    padding: 0;
    li {
      list-style: none;
      margin-right: 60px;
      cursor: pointer;
      :last-child {
        margin-right: 0;
      }

      a {
        width: 100%;
        height: 100%;
        padding: 10px;
        border-radius: 15px;
        box-sizing: border-box;
      }
    }
    .selected {
      ${selectedItemVariantStyles}
    }
  }

  svg {
    height: 25px;
  }

  ${paginatorVariantStyles}
`

const Paginator: FC<IPaginatorProps> = ({
  variant,
  scale,
  handlePageClick,
  pageRangeDisplay,
  pageSelected,
  totalElements,
  pageSize,
}) => {
  const [pageCount, setPageCount] = useState<number>(0)

  useEffect(() => {
    setPageCount(Math.ceil(totalElements / pageSize))
  }, [totalElements, pageSize])

  const onPageClick = useCallback(
    (event: { selected: number }) => {
      handlePageClick(event)
    },
    [handlePageClick],
  )

  return (
    <PaginatorContainer scale={scale} variant={variant} pageSelected={pageSelected}>
      <ReactPaginate
        breakLabel=""
        nextLabel={<LeftArrowSvg />}
        previousLabel={<RightArrowSvg />}
        onPageChange={onPageClick}
        forcePage={pageSelected}
        pageRangeDisplayed={pageRangeDisplay}
        pageCount={pageCount}
        marginPagesDisplayed={0}
      />
      <div className="total-container">
        <span>Total: </span>
        <div className="number-container">{pageCount}</div>
      </div>
    </PaginatorContainer>
  )
}

Paginator.defaultProps = {
  scale: scales.MD,
  variant: variants.MAIN,
}

export default Paginator
