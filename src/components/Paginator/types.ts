export enum variants {
  MAIN = 'MAIN',
}

export type Variants = keyof typeof variants

export enum scales {
  SM = 'SM',
  MD = 'MD',
  LG = 'LG',
}

export type Scales = keyof typeof scales

export interface IStyledPaginatorProps {
  scale?: Scales
  variant?: Variants
  pageSelected?: number
}

export interface IPaginatorProps extends IStyledPaginatorProps {
  handlePageClick: (selectedItem: { selected: number }) => void
  pageRangeDisplay: number
  pageSize: number
  totalElements: number
}
