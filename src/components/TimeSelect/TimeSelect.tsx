import React, { useCallback, useMemo } from 'react'
import styled from 'styled-components'

import { arrayPatternDate, datePattern } from './data'
import { DatePatternId, ITimeSelectProps, Scales, scales } from './types'

const getSize = (scale?: Scales) => {
  switch (scale) {
    case scales.SM:
      return `
       font-size: 14px;
       line-height: 20px;
       padding: 5px 10px;
       margin-right: 10px;
      `
    case scales.MD:
      return `
        font-size: 28px;
        padding: 10px;
      `
    case scales.LG:
    default:
      return `
        font-size: 32px;
        padding: 13px;
      `
  }
}

const TimeSelectItem = styled('div')<{ active?: boolean }>`
  border-radius: 20px;
  background: transparent;
  padding: 10px;
  margin-right: 50px;
  color: #fff;
  font-family: 'Maven Pro';
  font-size: 28px;
  font-weight: 600;
  line-height: 33px;
  text-align: center;
  cursor: pointer;

  ${({ active }) =>
    active &&
    `
    background: rgba(102, 252, 241, 0.1);
    color: #66fcf1;
    `}
`

const TimeSelectWrapper = styled('div')<{ scale?: Scales }>`
  width: 100%;
  display: flex;
  align-items: center;

  ${TimeSelectItem} {
    ${({ scale = scales.MD }) => getSize(scale)};
  }
`

const TimeSelect: React.FC<ITimeSelectProps> = ({ selectedPeriod, defaultDate, changeTimeSelect, scale, locale }) => {
  const onFilterCLick = useCallback(
    (name: DatePatternId) => {
      const convertedDate = datePattern(name)
      if (!convertedDate) return

      changeTimeSelect({
        period: name,
        dateFrom: convertedDate.date_from,
        dateTo: convertedDate.date_to,
      })
    },
    [changeTimeSelect],
  )

  const dateItems = useMemo(
    () =>
      arrayPatternDate.map(t => (
        <TimeSelectItem key={t.id} active={t.id === selectedPeriod} onClick={() => onFilterCLick(t.id)}>
          {locale ? locale[t.id] : t.name}
        </TimeSelectItem>
      )),
    [onFilterCLick, selectedPeriod, locale],
  )

  return (
    <TimeSelectWrapper scale={scale}>
      {dateItems}
      <TimeSelectItem onClick={() => changeTimeSelect(defaultDate)}>Clear date</TimeSelectItem>
    </TimeSelectWrapper>
  )
}

export default TimeSelect
