import dayjs from 'dayjs'
import { DatePatternId } from '../types'

interface IArrayPatternDate {
  id: DatePatternId
  name: string
}

export const arrayPatternDate: IArrayPatternDate[] = [
  { id: DatePatternId.TODAY, name: 'Today' },
  { id: DatePatternId.YESTERDAY, name: 'Yesterday' },
  { id: DatePatternId.THISWEEK, name: 'This Week' },
  { id: DatePatternId.LASTWEEK, name: 'Last Week' },
  { id: DatePatternId.THISMONTH, name: 'This Month' },
  { id: DatePatternId.LASTMONTH, name: 'Last Month' },
]

interface IDatePatternFnOutput {
  date_from: string
  date_to: string
}

export const datePattern = (param: DatePatternId): IDatePatternFnOutput | undefined => {
  switch (param) {
    case DatePatternId.TODAY:
      return {
        date_from: dayjs().startOf('day').format('YYYY-MM-DDTHH:mmZ'),
        date_to: dayjs().endOf('day').format('YYYY-MM-DDTHH:mmZ'),
      }
    case DatePatternId.YESTERDAY:
      return {
        date_from: dayjs().startOf('day').subtract(1, 'day').format('YYYY-MM-DDTHH:mmZ'),
        date_to: dayjs().endOf('day').subtract(1, 'day').format('YYYY-MM-DDTHH:mmZ'),
      }
    case DatePatternId.THISWEEK:
      return {
        date_from: dayjs().startOf('week').format('YYYY-MM-DDTHH:mmZ'),
        date_to: dayjs().endOf('week').format('YYYY-MM-DDTHH:mmZ'),
      }
    case DatePatternId.LASTWEEK:
      return {
        date_from: dayjs().startOf('week').subtract(1, 'week').format('YYYY-MM-DDTHH:mmZ'),
        date_to: dayjs().endOf('week').subtract(1, 'week').format('YYYY-MM-DDTHH:mmZ'),
      }
    case DatePatternId.THISMONTH:
      return {
        date_from: dayjs().startOf('month').format('YYYY-MM-DDTHH:mmZ'),
        date_to: dayjs().endOf('month').format('YYYY-MM-DDTHH:mmZ'),
      }
    case DatePatternId.LASTMONTH:
      return {
        date_from: dayjs().startOf('month').subtract(1, 'month').format('YYYY-MM-DDTHH:mmZ'),
        date_to: dayjs().endOf('month').subtract(1, 'month').format('YYYY-MM-DDTHH:mmZ'),
      }
    default:
      return undefined
  }
}
