export enum scales {
  SM = 'SM',
  MD = 'MD',
  LG = 'LG',
}

export type Scales = keyof typeof scales

export enum DatePatternId {
  TODAY = 'TODAY',
  YESTERDAY = 'YESTERDAY',
  THISWEEK = 'THISWEEK',
  LASTWEEK = 'LASTWEEK',
  THISMONTH = 'THISMONTH',
  LASTMONTH = 'LASTMONTH',
}

export type DatePatterIdType = keyof typeof DatePatternId

export interface IDefaultDate {
  period: DatePatterIdType
  dateFrom: string
  dateTo: string
}

export interface ITimeSelectProps {
  selectedPeriod: DatePatterIdType
  locale?: {
    [key in DatePatternId]: string
  }
  defaultDate: IDefaultDate
  changeTimeSelect: (time: IDefaultDate) => void
  scale?: Scales
}
