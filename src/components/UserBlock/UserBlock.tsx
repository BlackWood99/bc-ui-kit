import React from 'react'
import styled from 'styled-components'

import { ReactComponent as UserIcon } from 'shared/img/user/user.svg'

export interface IUserBlockProps {
  id: string | number
  role: string
  color: string
}

const IconWrapper = styled('div')`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 24px;
  width: 20px;
  margin-right: 2px;
`
const TextBlock = styled('div')`
  font-family: 'Maven Pro';
  letter-spacing: 0em;
  text-align: left;
  max-width: 100px;
`
const IdText = styled('div')`
  max-width: 100%;
  color: #fff;
  font-size: 14px;
  font-weight: 400;
  line-height: 14px;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`
const RoleText = styled('div')`
  max-width: 100%;
  font-size: 11px;
  font-weight: 700;
  line-height: 11px;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`

const BlockWrapper = styled('div')<{ color: string }>`
  display: flex;
  align-items: center;
  overflow: hidden;

  ${IconWrapper}, ${RoleText} {
    ${({ color }) =>
      color &&
      `
      & > svg * {
        fill: ${color};
      }

      color: ${color};
    `}
  }
`

const UserBlock: React.FC<IUserBlockProps> = ({ id, role, color }) => {
  return (
    <BlockWrapper color={color}>
      <IconWrapper>
        <UserIcon />
      </IconWrapper>
      <TextBlock>
        <IdText>{id}</IdText>
        <RoleText>{role}</RoleText>
      </TextBlock>
    </BlockWrapper>
  )
}

export default UserBlock
