import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { scales, variants } from '../components/Button/StandartButton/types'

import StandartButton from '../components/Button/StandartButton'

export default {
  title: 'Example/Button',
  component: StandartButton,
} as ComponentMeta<typeof StandartButton>

const Template: ComponentStory<typeof StandartButton> = args => <StandartButton {...args} />

export const MainButton = Template.bind({})
MainButton.args = {
  scale: scales.MD,
  variant: variants.MAIN,
  onClick: () => console.log(123),
  text: 'aboba',
  disabled: true
}

export const TestButton = Template.bind({})
TestButton.args = {
  scale: scales.MD,
  variant: variants.MAIN,
  onClick: () => console.log(2),
  text: 'aboba2',
}

export const TestButton2 = Template.bind({})
TestButton2.args = {
  scale: scales.MD,
  variant: variants.MAIN,
  onClick: () => console.log(2),
  text: 'aboba2',
}


