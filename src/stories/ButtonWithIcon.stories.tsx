import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { scales, variants } from '../components/Button/ButtonWithIcon/types'

import ButtonWithIcon from '../components/Button/ButtonWithIcon'

export default {
  title: 'Example/ButtonWithIcon',
  component: ButtonWithIcon,
} as ComponentMeta<typeof ButtonWithIcon>

const Template: ComponentStory<typeof ButtonWithIcon> = args => <ButtonWithIcon {...args} />

export const TestButton = Template.bind({})
TestButton.args = {
  scale: scales.MD,
  variant: variants.MAIN,
  onClick: () => console.log(123),
  text: 'aboba',
  defaultIcon: 'RELOAD',
  disabled: true,
}
