import React, { useState, FC } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { scales, variants, IChartHoverProps } from '../components/ChartHover/types'

import ChartHover from '../components/ChartHover'

const TestAboba: FC<IChartHoverProps> = ({ scale, variant, title, value, additionalText, bordered }) => {
  const [isVisible, setIsVisible] = useState(false)

  return (
    <>
    <ChartHover
      scale={scale}
      variant={variant}
      title={title}
      value={value}
      additionalText={additionalText}
      isVisible={isVisible}
      bordered={bordered}
    />
    <button onClick={() => setIsVisible(!isVisible)}>Включить</button>
    </>
  )
}

export default {
  title: 'Example/ChartHover',
  component: TestAboba,
} as ComponentMeta<typeof TestAboba>

const Template: ComponentStory<typeof TestAboba> = args => <TestAboba {...args} />

export const TestList = Template.bind({})

TestList.args = {
  scale: scales.MD,
  variant: variants.MAIN,
  title: 'Ховер',
  value: 400.45,
  additionalText: 'Aboba',
  bordered: false
}