import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import Checkbox from '../components/Checkbox/Checkbox'
import { scales } from '../components/Checkbox/types'
import InputGroup from '../components/Input/InputGroup'
import Input from '../components/Input/Input'

export default {
  title: 'Example/Checkbox',
  component: Checkbox,
} as ComponentMeta<typeof Checkbox>

const Template: ComponentStory<typeof Checkbox> = args => <Checkbox {...args} />

export const MainCheckbox = Template.bind({})
MainCheckbox.args = {
  scale: scales.MD,
}

const TemplateSecond: ComponentStory<typeof Checkbox> = args => (
  <InputGroup endIcon={<Checkbox {...args} />} textColor="#fff">
    <Input value="Checked" readOnly />
  </InputGroup>
)
export const CheckboxWithInput = TemplateSecond.bind({})
CheckboxWithInput.args = {
  scale: scales.MD,
  checked: true,
  onChange: (fn: any) => fn,
}
