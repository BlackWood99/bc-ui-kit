import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { scales } from '../components/Input/types'
import DatePicker from '../components/DatePicker/DatePicker'

export default {
  title: 'Example/DatePicker',
  component: DatePicker,
} as ComponentMeta<typeof DatePicker>

const Template: ComponentStory<typeof DatePicker> = args => <DatePicker {...args} />

export const MainDatePicker = Template.bind({})
MainDatePicker.args = {
  selected: new Date(),
  onChange: (date: Date) => console.log(date),
  wrapperProps: {
    scale: scales.MD,
    iconColor: '#fff',
  },
}
