import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import Input from '../components/Input/Input'
import { scales, variants } from '../components/Input/types'

export default {
  title: 'Example/Input',
  component: Input,
} as ComponentMeta<typeof Input>

const Template: ComponentStory<typeof Input> = args => <Input {...args} />

export const MainInput = Template.bind({})
MainInput.args = {
  scale: scales.MD,
  variant: variants.MAIN,
  placeholder: 'some text',
}

export const TestInput = Template.bind({})
TestInput.args = {
  scale: scales.LG,
  variant: variants.FILLED,
  placeholder: 'type text',
  onChange: e => console.log(e.currentTarget.value),
}
