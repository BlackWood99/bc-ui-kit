import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import Input from '../components/Input/Input'
import InputGroup from '../components/Input/InputGroup'
import { scales, variants } from '../components/Input/types'
import { ReactComponent as EyeIcon } from 'shared/img/eye.svg'

export default {
  title: 'Example/InputGroup',
  component: InputGroup,
} as ComponentMeta<typeof InputGroup>

const Template: ComponentStory<typeof InputGroup> = args => {
  return (
    <InputGroup {...args}>
      <Input />
    </InputGroup>
  )
}

export const TestInputGroup = Template.bind({})
TestInputGroup.args = {
  scale: scales.MD,
  endIcon: <EyeIcon />,
}

export const AdditionalInput = Template.bind({})
AdditionalInput.args = {
  scale: scales.MD,
  variant: variants.SECONDARY,
  endIcon: <span>TND</span>,
  iconColor: '#fff',
  textColor: '#fff',
}
