import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import InputPassword from '../components/Input/InputPassword'
import { scales } from '../components/Input/types'

export default {
  title: 'Example/InputPassword',
  component: InputPassword,
} as ComponentMeta<typeof InputPassword>

const Template: ComponentStory<typeof InputPassword> = args => <InputPassword {...args} />

export const PasswordInput = Template.bind({})
PasswordInput.args = {
  scale: scales.MD,
}
