import React, { useState, FC } from 'react'
import styled from 'styled-components'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { scales, variants, IListLikeButtonProps } from '../components/List/ListLikeButton/types'

import ListLikeButton from '../components/List/ListLikeButton'

const Container = styled('div')`
  width: 171px;
`

const TestAboba: FC<IListLikeButtonProps> = ({
  scale,
  variant,
  placeHolder,
  elements,
  defaultIcon,
  displayPlaceHolder,
}) => {
  const [selecteValue, setSelectedValue] = useState('')

  const onChangeValue = (item: string) => {
    setSelectedValue(item)
  }
  return (
    <Container>
      <ListLikeButton
        scale={scale}
        variant={variant}
        placeHolder={placeHolder}
        elements={elements}
        defaultIcon={defaultIcon}
        value={selecteValue}
        onChange={onChangeValue}
        displayPlaceHolder={displayPlaceHolder}
      />
    </Container>
  )
}

export default {
  title: 'Example/ListLikeButton',
  component: TestAboba,
} as ComponentMeta<typeof TestAboba>

const Template: ComponentStory<typeof TestAboba> = args => <TestAboba {...args} />

export const TestList = Template.bind({})

TestList.args = {
  scale: scales.MD,
  variant: variants.MAIN,
  placeHolder: 'Language',
  elements: ['English', 'Russian', 'Franc'],
  defaultIcon: 'LANGUAGE',
  displayPlaceHolder: true,
}
