import React, { useState, FC } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { scales, variants, IListLikeFieldProps } from '../components/List/ListLikeField/types'

import ListLikeField from '../components/List/ListLikeField'

const TestAboba: FC<IListLikeFieldProps> = ({ scale, variant, placeHolder, elements, defaultIcon }) => {
  const [selecteValue, setSelectedValue] = useState('')

  const onChangeValue = (item: string) => {
    setSelectedValue(item)
  }
  return (
    <ListLikeField
      scale={scale}
      variant={variant}
      placeHolder={placeHolder}
      elements={elements}
      defaultIcon={defaultIcon}
      value={selecteValue}
      onChange={onChangeValue}
    />
  )
}

export default {
  title: 'Example/ListLikeField',
  component: TestAboba,
} as ComponentMeta<typeof TestAboba>

const Template: ComponentStory<typeof TestAboba> = args => <TestAboba {...args} />

export const TestList = Template.bind({})

TestList.args = {
  scale: scales.MD,
  variant: variants.MAIN,
  placeHolder: 'listLikeField',
  elements: ['item1', 'item2', 'item3'],
  defaultIcon: 'PERSON',
}
