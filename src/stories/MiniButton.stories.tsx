import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { scales, variants, defaultIcons } from '../components/Button/MiniButton/types'

import MiniButton from '../components/Button/MiniButton'
import {ReactComponent as Calendar} from '../shared/img/buttons/Calendar.svg'

export default {
  title: 'Example/MiniButton',
  component: MiniButton,
} as ComponentMeta<typeof MiniButton>

const Template: ComponentStory<typeof MiniButton> = args => <MiniButton {...args} />

export const TestButton1 = Template.bind({})
TestButton1.args = {
  scale: scales.MD,
  variant: variants.MAIN,
  onClick: () => console.log(123),
  text: 'aboba',
  defaultIcon: defaultIcons.DELETE,
}
