import React, { useState, FC, useEffect } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { scales, variants, IPaginatorProps } from '../components/Paginator/types'

import Paginator from '../components/Paginator'

const items = [...Array(201).keys()]

const Items = ({ currentItems }: { currentItems: number[] }) => {
  return <div>{currentItems && currentItems.map(item => <div>{item}</div>)}</div>
}

const TestAboba: FC<IPaginatorProps> = ({ scale, variant, pageRangeDisplay }) => {
  const [currentPage, setCurrentPage] = useState(0)
  const [currentItems, setCurrentItems] = useState<number[]>([])
  const [itemOffset, setItemOffset] = useState(0)
  const itemsPerPage = 5

  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage
    setCurrentItems(items.slice(itemOffset, endOffset))
  }, [itemOffset, itemsPerPage])

  const handlePageClick = (event: any) => {
    setCurrentPage(event.selected)
    const newOffset = (event.selected * itemsPerPage) % items.length
    setItemOffset(newOffset)
  }

  return (
    <>
      <Items currentItems={currentItems} />
      <Paginator
        scale={scale}
        variant={variant}
        handlePageClick={handlePageClick}
        pageRangeDisplay={pageRangeDisplay}
        pageSelected={currentPage}
        totalElements={items.length}
        pageSize={itemsPerPage}
      />
      <button onClick={() => setCurrentPage(15 - 1)}>click</button>
    </>
  )
}

export default {
  title: 'Example/Paginator',
  component: TestAboba,
} as ComponentMeta<typeof TestAboba>

const Template: ComponentStory<typeof TestAboba> = args => <TestAboba {...args} />

export const TestList = Template.bind({})

TestList.args = {
  scale: scales.MD,
  variant: variants.MAIN,
  pageRangeDisplay: 9,
}
