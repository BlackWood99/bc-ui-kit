import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import TimeSelect from '../components/TimeSelect/TimeSelect'

export default {
  title: 'Example/TimeSelect',
  component: TimeSelect,
} as ComponentMeta<typeof TimeSelect>

const Template: ComponentStory<typeof TimeSelect> = args => <TimeSelect {...args} />

export const MainTimeSelect = Template.bind({})
MainTimeSelect.args = {
  selectedPeriod: 'THISMONTH',
  defaultDate: {
    period: 'THISMONTH',
    dateFrom: '',
    dateTo: '',
  },
  changeTimeSelect: time => console.log('time', time),
}
