import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import UserBlock from '../components/UserBlock/UserBlock'

export default {
  title: 'Example/UserBlock',
  component: UserBlock,
} as ComponentMeta<typeof UserBlock>

const Template: ComponentStory<typeof UserBlock> = args => <UserBlock {...args} />

export const MainUserBlock = Template.bind({})
MainUserBlock.args = {
  id: 15,
  role: 'Admin',
  color: 'red',
}
