export const buttonColors = {
    main: '#46A29F',
    success: '#11C023',
    danger: '#C01E1E',
    textCommon: '#FFF',
    miniButtonMainOpacity: 'rgba(70, 162, 159, 0.1)',
    miniButtonDangerOpacity: 'rgba(192, 30, 30, 0.1)',
    disabledBackground: 'rgba(74, 74, 74, 0.2)',
    disbledBorder: 'rgba(197, 198, 200, 0.25)',
    disabledColor: 'rgba(197, 198, 200, 0.25)',
    disabledBoxShadow: '0px 4px 14px rgba(0, 0, 0, 0.25)',
}