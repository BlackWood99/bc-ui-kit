export const chartHoverColors = {
    mainBackground: '#46A29F',
    mainText: '#FFFFFF',
    mainCircle: '#C01E1E',
    mainBorder: '#C5C6C8',
    mainAdditionalText: '#C5C6C8'
}