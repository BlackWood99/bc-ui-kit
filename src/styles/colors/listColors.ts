export const listColors = {
    mainText: '#FFFFFF',
    itemText: '#0B0C10',
    mainBorder: '#C5C6C8',
    mainItemBackground: '#66FCF1',
    mainListHeaderBackground: '#46A29F',
    mainItemSelectedBackground: '#202833',
    mainItemHoverBackground: '#46A29F',
}