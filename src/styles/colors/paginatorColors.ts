export const paginatorColors = {
    mainBackground: '#202833',
    mainText: '#FFFFFF',
    mainSelectedItemBackground: 'rgba(70, 162, 159, 0.1)',
    mainSelectedItemColor: '#46A29F'
}